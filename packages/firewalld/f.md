
## 虚拟机搭建

1. 虚拟机使用NAT或者桥接
2. .env中HOST_IP为vmware为Linux分配的IP
3. CentOS7.6 搭建注意
	CentOS7.6默认采用firewalld防火墙，docker启动后容器可以ping通宿主机，但是docker无法访问映射到宿主机端口上的http服务，使用以下命令修复。其中`172.18.0.0/16`是docker-compose启动的容器所在的网段。
	```
	firewall-cmd --permanent --zone=trusted --change-interface=docker0
	firewall-cmd --permanent --zone=trusted --add-port=18006/tcp
	firewall-cmd --permanent --zone=public --add-rich-rule='rule family=ipv4 source address=172.18.0.0/16 accept' // 最重要
	firewall-cmd --reload
	```


# 说明

- 拉取
    ```
        sshpass -p "public" rsync -a public@172.16.1.188:/Users/pub/share/yunke_dev_env .
    ```
