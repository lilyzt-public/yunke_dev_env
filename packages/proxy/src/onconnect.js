var debug = require('debug')('proxy')

var net = require('net')
// var url = require('url');
var http = require('http')

// log levels
debug.request = require('debug')('proxy ← ← ←')
debug.response = require('debug')('proxy → → →')
debug.proxyRequest = require('debug')('proxy ↑ ↑ ↑')
debug.proxyResponse = require('debug')('proxy ↓ ↓ ↓')

function onconnect (req, socket, head) {
  debug.request('%s %s HTTP/%s ', req.method, req.url, req.httpVersion)

  var res
  var target
  var gotResponse = false

  function onclientclose (err) {
    debug.request('HTTP request %s socket "close" event', req.url)
  }
  socket.on('close', onclientclose)

  function onclientend () {
    debug.request('HTTP request %s socket "end" event', req.url)
    cleanup()
  }

  function onclienterror (err) {
    debug.request('HTTP request %s socket "error" event: \n%s', req.url, err.stack || err)
  }
  socket.on('error', onclienterror)

  function ontargetclose () {
    debug.proxyResponse('proxy target %s "close" event', req.url)
    cleanup()
    socket.destroy()
  }

  function ontargetend () {
    debug.proxyResponse('proxy target %s "end" event', req.url)
    cleanup()
  }

  function ontargeterror (err) {
    debug.proxyResponse('proxy target %s "error" event:\n%s', req.url, err.stack || err)
    cleanup()
    if (gotResponse) {
      debug.response('already sent a response, just destroying the socket...')
      socket.destroy()
    } else if (err.code == 'ENOTFOUND') {
      debug.response('HTTP/1.1 404 Not Found')
      res.writeHead(404)
      res.end()
    } else {
      debug.response('HTTP/1.1 500 Internal Server Error')
      res.writeHead(500)
      res.end()
    }
  }

  function ontargetconnect () {
    debug.proxyResponse('proxy target %s "connect" event', req.url)
    debug.response('HTTP/1.1 200 Connection established')
    gotResponse = true
    res.removeListener('finish', onfinish)

    res.writeHead(200, 'Connection established')

    // HACK: force a flush of the HTTP header
    res._send('')

    // relinquish control of the `socket` from the ServerResponse instance
    res.detachSocket(socket)

    // nullify the ServerResponse object, so that it can be cleaned
    // up before this socket proxying is completed
    res = null

    socket.pipe(target)
    target.pipe(socket)
  }

  // cleans up event listeners for the `socket` and `target` sockets
  function cleanup () {
    debug.response('cleanup')
    socket.removeListener('close', onclientclose)
    socket.removeListener('error', onclienterror)
    socket.removeListener('end', onclientend)
    if (target) {
      target.removeListener('connect', ontargetconnect)
      target.removeListener('close', ontargetclose)
      target.removeListener('error', ontargeterror)
      target.removeListener('end', ontargetend)
    }
  }

  // create the `res` instance for this request since Node.js
  // doesn't provide us with one :(
  // XXX: this is undocumented API, so it will break some day (ノಠ益ಠ)ノ彡┻━┻
  res = new http.ServerResponse(req)
  res.shouldKeepAlive = false
  res.chunkedEncoding = false
  res.useChunkedEncodingByDefault = false
  res.assignSocket(socket)

  function onfinish () {
    debug.response('response "finish" event')
    res.detachSocket(socket)
    socket.end()
  }
  res.once('finish', onfinish)

  // pause the socket during authentication so no data is lost
  socket.pause()

  socket.resume()
  var parts = req.url.split(':')
  var host = parts[0]
  var port = +parts[1]
  var opts = { host: host, port: port }

  debug.proxyRequest('connecting to proxy target %j', opts)
  target = net.connect(opts)
  target.on('connect', ontargetconnect)
  target.on('close', ontargetclose)
  target.on('error', ontargeterror)
  target.on('end', ontargetend)
}

module.exports = onconnect
