var url = require('url')
var http = require('http')
var express = require('express')
var onconnect = require('./onconnect')
const httpProxyMiddleware = require('http-proxy-middleware')

const app = new express()
const server = http.createServer(app)
const debug = require('debug')('proxy)')
debug.request = require('debug')('proxy ← ← ←')
debug.response = require('debug')('proxy → → →')
debug.proxyRequest = require('debug')('proxy ↑ ↑ ↑')
debug.proxyResponse = require('debug')('proxy ↓ ↓ ↓')

function eachHeader (obj, fn) {
  if (Array.isArray(obj.rawHeaders)) {
    // ideal scenario... >= node v0.11.x
    // every even entry is a "key", every odd entry is a "value"
    var key = null
    obj.rawHeaders.forEach(function (v) {
      if (key === null) {
        key = v
      } else {
        fn(key, v)
        key = null
      }
    })
  } else {
    // otherwise we can *only* proxy the header names as lowercase'd
    var headers = obj.headers
    if (!headers) return
    Object.keys(headers).forEach(function (key) {
      var value = headers[key]
      if (Array.isArray(value)) {
        // set-cookie
        value.forEach(function (val) {
          fn(key, val)
        })
      } else {
        fn(key, value)
      }
    })
  }
}

const proxyConfig = [
  {
    context: '/api',
    target: 'http://dev-qmyx-cg.myscrm.cn:18006'
  }
]

const getProxyMiddleware = (proxyConfig) => {
  const context = proxyConfig.context || proxyConfig.path
  // It is possible to use the `bypass` method without a `target`.
  // However, the proxy middleware has no use in this case, and will fail to instantiate.
  if (proxyConfig.target) {
    return httpProxyMiddleware(context, proxyConfig)
  }
}

proxyConfig.forEach(config => {
  const proxyMiddleware = getProxyMiddleware(config)
  app.use((req, res, next) => {
    if (proxyMiddleware) {
      return proxyMiddleware(req, res, next)
    } else {
      next()
    }
  })
})

app.all('*', function (req, res, next) {
  console.log('*: ', req.url)
  req.socket.on('error', function () {
    console.log('request socket error:', err)
  })

  var socket = req.socket

  socket.pause()
  socket.resume()

  var parsed = url.parse(req.url)
  parsed.method = req.method
  parsed.headers = req.headers
  var proxyReq = http.request(parsed)

  proxyReq.on('response', function (proxyRes) {
    var headers = {}
    eachHeader(proxyRes, function (key, value) {
      // debug.proxyResponse('Proxy Response Header: "%s: %s"', key, value);
      // if (isHopByHop.test(key)) {
      // debug.response('ignoring hop-by-hop header "%s"', key);
      // } else {
      var v = headers[key]
      if (Array.isArray(v)) {
        v.push(value)
      } else if (v != null) {
        headers[key] = [v, value]
      } else {
        headers[key] = value
      }
      // }
    })
    res.writeHead(proxyRes.statusCode, headers)
    proxyRes.pipe(res)
    res.on('finish', onfinish)
  })
  proxyReq.on('error', function (err) {
    debug.proxyResponse('proxy HTTP request "error" event\n%s', err.stack || err)
  })
  req.pipe(proxyReq)

  function onclose () {
    debug.request('client socket "close" event, aborting HTTP request to "%s"', req.url)
    proxyReq.abort()
    cleanup()
  }
  socket.on('close', onclose)
  function onfinish () {
    debug.response('"finish" event')
    cleanup()
  }

  function cleanup () {
    debug.response('cleanup')
    socket.removeListener('close', onclose)
    res.removeListener('finish', onfinish)
  }
})

server.on('connect', onconnect)

server.listen(9999, function () {
  var port = server.address().port
  console.log('HTTP(s) proxy server listening on port %d', port)
})
