# 安装
## Docker安装
### Mac/Linux
1. 搭建Docker环境
    1. 为加快构建速度，请使用国内docker镜像，比如阿里云
    2. 项目中使用到以下两个镜像，可先pull下来加快`build`速度
        `docker pull node:8.16-jessie`
        `docker pull node:10.15.3-jessie`
        `docker pull centos:7.2.1511`
    3. Node镜像基于Debian，Dockerfile中没有配置国内源，出现构建太慢的情况请 保证网络良好或自行配置软件源。
2. 修改.env仓库位置
    1. 变量的意义
        GIT_REPOS： 云客所有仓库位置
        ENV_PKGS： 本项目内的代码位置 ./packages/
        HOST_IP： Docker宿主机IP地址
3. ~~下载Charles，导入`packages/charles/rewrite.xml`~~
4. 在宿主机`/etc/hosts`中**append**`packages/hosts/hosts`文件内容
5. 构建镜像
    ```
        docker-compose -f dev.yml build
    ```
6. 启动容器
    ```
        docker-compose -f dev.yml up
    ```
7. 重启
    前端代码有的时候更改后无法即时重新构建，这个时候
    ```
    docker-compose -f dev.yml restart p_yunke_back
    ```
    
### Windows
1. 使用`git-for-windows`
2. git全局配置
    ```
    git config --global core.eol lf
    git config --global core.autocrlf input
    ```
3. exec-user-process-caused-no-such-file-or-directory
    安装`notepad++`
    compose/dev下所有entrypoint.sh文件
    编辑、文档格式转换、转为Unix

4. Windows上容器出现奇怪的问题，实在不行就在虚拟机中搭建(浏览器代理指向虚拟机IP)。

## 虚拟机安装
再增加虚拟机安装之前，已有同学使用虚拟机安装，但是由于不同人都有自己喜欢和熟悉的发行版，发行版的不同，出现的问题不同，所以这里直接提供一份CentOS虚拟机，无需再担心环境问题。心有余力的同学，可以使用自己喜欢的发行版，按照`Docker安装`中的步骤自行搭建环境。

### devCentOS6.7说明

- SELinux
    必须关闭，否则莫名其妙的问题
- VMware
    基于Workstation 12.x
- 网络
    桥接
- 用户
    yunkedev/yunkedev
    root/root
- 共享
    open-vm-tools
- 软件/命令
    docker
        存储采用overlay，默认overlay2有问题
    tree
    vim
- 服务
    Samba
        用户： yunkedev/yunkedev
        暴露了: /home/yunkedev和/目录


### devCentOS6.7的使用
1. 下载复制虚拟机
2. Vmware中扫描新增该虚拟机
3. 启动后会给虚拟机分配一个新的桥接IP
4. 在/home/yunkedev/workspace/corp/Yunke/yunke_dev_env中按照`Docker安装` 2、5、6步启动docker


# 使用
1. 浏览器配置代理
    1. 安装`SwitchyOmega`插件
    2. 新建代理配置项

        node-proxy
            HTTP 127.0.0.1 9999
2. 浏览器访问
    - 云客后台
        http://dev-qmyx-cg.myscrm.cn:8006
            hzzhongxadmin
            mysoftadmin
            1
    - 案场助理
    - 移动销售
