#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

cmd="$@"
# 
[ ! -d /webser ] && mkdir /webser
chown -R nginx.nginx /webser
chown -R nginx.nginx /yunke
cp -fa /yunke/yunke-public /yunke/yunkerefactor/publiccode

# php-fpm
/usr/sbin/php-fpm -D
# nginx
/usr/sbin/nginx -g "daemon off;"

exec $cmd