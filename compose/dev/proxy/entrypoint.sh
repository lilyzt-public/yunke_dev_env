#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

cmd="$@"
# 
cd /yunke/proxy
yarn
npm start

exec $cmd