#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

cmd="$@"
# 
cd /yunke/p_yunke_back
echo "0.0.0.0  dev-qmyx-cg.myscrm.cn" >> /etc/hosts
yarn
npm start

exec $cmd